<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>OpenSlackMap</title>
        <link rel="shortcut icon" href="images/openslackmap.ico" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/leaflet.css" />
        <link rel="stylesheet" href="css/L.Control.MousePosition.css" />
        <link rel="stylesheet" href="css/leaflet.label.css" />
        <link rel="stylesheet" href="css/Control.Geocoder.css" />
        <link rel="stylesheet" href="css/leaflet-sidebar.css" />
        <link rel="stylesheet" href="css/Control.MiniMap.css" />
        <link rel="stylesheet" href="css/jquery-ui.min.css" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="js/tablesorter/themes/blue/style.css" />
        <link rel="stylesheet" href="css/Leaflet.Elevation-0.0.2.css" />
        <link rel="stylesheet" href="css/leaflet-routing-machine.css" />
        <link rel="stylesheet" href="css/leaflet.iconlabel.css" />
        <link rel="stylesheet" href="css/jquery.custom-scrollbar.css" />
        <link rel="stylesheet" type="text/css" href="css/openslackmap.css" />

        <link rel="stylesheet" href="css/MarkerCluster.css" />
        <link rel="stylesheet" href="css/MarkerCluster.Default.css" />
        <link rel="stylesheet" href="css/L.Control.Locate.min.css" />
    </head>
    <body>
<?php
require('conf.php');

?>

 <div id="sidebar" class="sidebar">
<!-- Nav tabs -->
<ul class="sidebar-tabs" role="tablist">
<li class="active"><a href="#home" role="tab"><i class="fa fa-bars"></i></a></li>
<li><a href="#settings" role="tab"><i class="fa fa-gear"></i></a></li>
<li><a href="#help" role="tab"><i class="fa fa-question"></i></a></li>
</ul>
<!-- Tab panes -->
<div class="sidebar-content active">
<div class="sidebar-pane active" id="home">
            <div id="logo">
                <div style='height:100%;width:33%;float:left'><br/><b>OpenSlack Map</b></div>
                <img style='width:100px;height:auto;float:left' src="images/openslackmap.png"/>
                <div style='text-align:right;height:100%;width:33%;float:left'><br/><b>Slackline spot databases</b></div>
            </div>
            <hr />
<?php
echo 'Last update : '. date("d F Y H:i:s", filemtime('slackmarkers.txt'));
?>
            <hr />
            <div id="folderselection">
                    <div id="folderrightdiv">
                    <select name="category" id="categoryselect">
                        <option>All categories</option>
                        <option>Waterline only</option>
                        <option>Highline only</option>
                        <option>Longline only</option>
                        <option>Slackline only</option>
                    </select>
                    </div>
                    <div id="folderleftdiv">
                        <b id="titlechoosedirform">Display : </b>
                        <br/>
                    </div>

            </div>
            <div style="clear:both"></div>
            <hr/-->
            <h3>Lines inside current view</h3>
            <div id="linelist"></div>
<?php

    echo "<script> watermarkers = ".file_get_contents("watermarkers.txt")."</script>\n";
    echo "<script> highmarkers = ".file_get_contents("highmarkers.txt")."</script>\n";
    echo "<script> longmarkers = ".file_get_contents("longmarkers.txt")."</script>\n";
    echo "<script> slackmarkers = ".file_get_contents("slackmarkers.txt")."</script>\n";
    echo "<script> reuwatermarkers = ".file_get_contents("reuwatermarkers.txt")."</script>\n";
    echo "<script> reuhighmarkers = ".file_get_contents("reuhighmarkers.txt")."</script>\n";
    echo "<script> reulongmarkers = ".file_get_contents("reulongmarkers.txt")."</script>\n";
    echo "<script> reumidmarkers = ".file_get_contents("reumidmarkers.txt")."</script>\n";
?>
</div>
<div class="sidebar-pane" id="settings">
<br/>
<div id="filtertabtitle">
    <b>Filters</b>
    <button id="clearfilter" class="uibutton">Clear</button>
    <button id="applyfilter" class="uibutton">Apply</button>
</div>
<br/>
<br/>
<br/>
<ul id="filterlist">
    <li>
        <b>Length (m)</b><br/>
        min : <input id="lenmin"><br/>
        max : <input id="lenmax">
    </li>
    <li>
        <b>Height (m)</b><br/>
        min : <input id="heimin"><br/>
        max : <input id="heimax">
    </li>
</ul>
            <hr/>
                    <a href='getGpxFile.php?line=reuhigh'>Get Reunion island highline database gpx file</a>
                    <br/>
                    <a href='getGpxFile.php?line=reulong'>Get Reunion island longline database gpx file</a>
                    <br/>
                    <a href='getGpxFile.php?line=reumid'>Get Reunion island midline database gpx file</a>
                    <br/>
                    <a href='getGpxFile.php?line=reuwater'>Get Reunion island waterline database gpx file</a>
                    <br/>
                    <a href='getGpxFile.php?line=high'>Get highline database gpx file</a>
                    <br/>
                    <a href='getGpxFile.php?line=water'>Get waterline database gpx file</a>
                    <br/>
                    <a href='getGpxFile.php?line=long'>Get longline database gpx file</a>
                    <br/>
                    <a href='getGpxFile.php?line=slack'>Get slackline database gpx file</a>
            <hr/>
            <div id="options">
                <h3>Options</h3>
                <!--div id="optionbuttonsdiv">
                    <button id='removeelevation' class="uibutton">Hide elevation profile</button>
                    <br/>
                    <button id='comparebutton'  class="uibutton">Compare selected tracks</button>
                </div-->
                <div id="optioncheckdiv">
                    <!--input id='displayclusters' type='checkbox' checked='checked'><label for='displayclusters'>Display markers</label>
                    <br/-->
                    <input id='transparentcheck' type='checkbox' title="Enables transparency when hover on table rows"><label title="Enables transparency when hover on table rows" for='transparentcheck'>Transparency</label>
                    <br/>
                    <!--label for='colorcriteria' title='Enables tracks coloring by the chosen criteria'>Color by :</label>
                    <select name="colorcriteria" title='Enables tracks coloring by the chosen criteria' id="colorcriteria">
                        <option>none</option>
                        <option>speed</option>
                        <option>slope</option>
                        <option>elevation</option>
                    </select-->
                </div>
            </div>
            <div style="clear:both"></div>
</div>
<div class="sidebar-pane" id="help"><h1>Help</h1>
    This website is running a free software. The project page is <a href='https://gitlab.com/eneiluj/openslackmap'>here.</a> You have the right to :
    <ul>
        <li>Host it</li>
        <li>Use it</li>
        <li>Study it</li>
        <li>Modify it</li>
        <li>Redistribute it</li>
    </ul>

    <br/>

    All data is daily imported from <a href='http://highlinedatabase.blogspot.fr/'>highlinedatabase</a>, <a href='http://waterlinedatabase.blogspot.fr/'>waterlinedatabase</a>, <a href="http://slack-mountain.com/shop/fr/content/14-carte">Slack-Mountain</a> and <a href="http://www.slackline974.org/map/">Slackline974</a>.
Huge thanks to them for letting me use/modify/normalize their content.
<br/><br/>

    Shortcuts :
    <ul>
        <li>&lt; : toggle sidebar</li>
        <li>! : toggle minimap</li>
        <li>œ or ² : toggle search</li>
    </ul>
    <br/>
    Features :
    <ul>
        <li>Click on marker cluster to zoom in.</li>
        <li>Click on track marker to show popup with line infos and a link to line description.</li>
        <li>In main sidebar tab, the table lists all lines that fit into current map bounds. This table is kept up to date.</li>
        <li>Sidebar table columns are sortable.</li>
        <li>In sidebar table, [p] link near the track name is a permalink.</li>
        <li>Many leaflet plugins are active :
            <ul>
                <li>Markercluster</li>
                <li>Elevation</li>
                <li>Sidebar-v2</li>
                <li>Control Geocoder (search in nominatim DB)</li>
                <li>Minimap (bottom-left corner of map)</li>
                <li>MousePosition</li>
            </ul>
        </li>
    </ul>
</div>
</div>
</div>
</div>
<!-- ============================ -->

        <div id="map" class="sidebar-map"></div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js" charset="utf-8"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/leaflet.js"></script>
        <script src="js/leaflet.label.js"></script>
        <script src="js/L.Control.MousePosition.js"></script>
        <script src="js/Control.Geocoder.js"></script>
        <script src="js/Control.MiniMap.js"></script>
        <script src="js/L.Control.Locate.min.js"></script>
        <script src="js/leaflet-sidebar.min.js"></script>
        <!--script type="text/javascript" src="http://maps.stamen.com/js/tile.stamen.js?v1.3.0"></script-->
        <script src="js/leaflet.markercluster-src.js"></script>
        <script src="js/Leaflet.Elevation-0.0.2.min.js"></script>
        <script src="js/jquery-1.10.2.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/jquery.mousewheel.js"></script>
        <script src="js/leaflet-routing-machine.js"></script>
        <script src="js/leaflet.iconlabel.js"></script>
        <script src="js/L.activearea.js"></script>
        <script src="js/jquery.custom-scrollbar.js"></script>
        <script src="js/tablesorter/jquery.tablesorter.min.js"></script>
        <script src="js/openslackmap.js"></script>
    </body>
</html>
